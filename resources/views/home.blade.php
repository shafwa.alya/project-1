@extends('layouts.app')

@section('content')
 <!-- desktop -->
 <div class="d-md-block d-none">
    <div class="row mt-4">
        <div class="col-md-6">
            <div class="d-flex h-100">
                <div class="justify-content-center align-self-center">
                    <h1>
                        <strong>SharePost</strong><br /><br>
                    </h1>
                    <h2>"SharePost" Berbagi ceritamu hari ini bersama komunitas</h2>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <img src="{{ asset('images/undraw/undraw_Connected_re_lmq2.png') }}" width="100%" />
        </div>
    </div>
</div>

<!-- handphone -->
<div class="d-sm-block d-md-none">
    <div class="row mt-4">
        <div class="col-md-6 mb-3">
            <img src="{{ asset('images/undraw/undraw_Connected_re_lmq2.png') }}" width="100%" />
        </div>
        <div class="col-md-6">
            <div class="d-flex h-100">
                <div class="justify-content-center align-self-center">
                    <h1>
                        <strong>SharePost</strong><br />
                    </h1>
                    <h2>"SharePost" Berbagi ceritamu hari ini bersama komunitas</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mb-5 mt-5">
    <div class="col-md-12">
        <div class="card card_grey">
            <div class="text-center p-4">
                <h1 style="font-weight: bold;">{{$user_count}}</h1>
                <h4>ACCOUNT HAS BEEN REGISTERED ON SHAREPOST</h4>
            </div>
        </div>
    </div>
</div>

<div class="row m-5 justify-content-center">
    <div class="col-md-2 mb-2 mb-4 mt-5">
        <h2 class="title_border">About Us</h2>
    </div>
    <div class="row">
        <div class="col-md-8">
                <b>SharePost</b> adalah aplikasi jejaring sosial yang memudahkan para penggunanya untuk melakukan interaksi sosial secara online. 
           
        </div>
        <div class="col-md-4">
            Melalui <b>SharePost</b>, seseorang bisa berbagi foto dan  membuat cerita singkat dengan teman-teman.
        </div>
    </div>
</div>

<div class="row m-5 justify-content-center">
    <div class="col-md-2 mb-2 mb-4 mt-5">
        <h2 class="title_border">Our Team</h2>
    </div>
   <div class="row">
    <div class="col-md-4 mb-3">
        <div class="card">
            <img class="card-img-top" src="{{asset('images/defaultprofile.png')}}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title text-center">Bertho Erizal</h5>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-3">
        <div class="card">
            <img class="card-img-top" src="{{asset('images/defaultprofile.png')}}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title text-center">Alya Shafwa Wafiya</h5>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-3">
        <div class="card">
            <img class="card-img-top" src="{{asset('images/defaultprofile.png')}}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title text-center">Anton Kartono</h5>
            </div>
        </div>
    </div>
   </div>
</div>

@endsection
