<!-- Button trigger modal -->
<button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#exampleModal">
    Edit Password
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('update_password', [Auth::user()->id])}}" method="post">
          @method('put')
          @csrf
        <div class="modal-body">
          <div class="form-group">
              <label for="password">New Password</label>
              <input type="password" class="form-control" name="password" id="password">
              @error('password')
                  <span class="text-danger">{{$message}}</span>
              @enderror
          </div>
          <div class="form-group">
              <label for="password_confirmation">Confirmation Password</label>
              <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
      </div>
    </div>
  </div>