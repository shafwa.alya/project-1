@extends('layouts.app')

@section('content')
<h1>{{$user->username}}</h1>
<hr>
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif

@foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $error }}</strong>
    </div>
@endforeach
<div class="row">
    <div class="col-md-3 mb-3">
        <div class="card">
                <img class="card-img-top" src="{{asset('images/'.$user->picture)}}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">{{$user->fname}} {{$user->lname}}</h5>
              <hr>
                <span class="badge badge-light badge_grey">
                    @if ($user->sex=='male')
                    Male
                    @else
                    Female
                    @endif
                </span>
                <span class="badge badge-light badge_grey">
                    <?php echo date("Y")-date("Y", strtotime($user->birthday)); ?> Years Old
                </span>
            </div>
            @if ($user->id!=Auth::user()->id)
                @if ($checkfollow==1)
                <a href="/follow/unfollow/{{$user->users_id}}" class="btn btn-warning btn-block">Unfollow</b></a>
                @else
                <a href="/follow/following/{{$user->users_id}}" class="btn btn-success btn-block">Follow</b></a>
                @endif
            @endif
            <button type="button" class="btn btn-primary btn-block" disabled>Followers <b>{{$count_followers}}</b></button>
            <button type="button" class="btn btn-primary btn-block" disabled>Following <b>{{$count_following}}</b></button>
            @if ($user->id==Auth::user()->id)
            <a href="/users/{{$user->id}}/edit" class="btn btn-warning btn-block">Edit Account</a>
            @include('user.edit_password')
            @include('user.delete')
            @endif
        </div>
    </div>
    <div class="col-md-9">
        <div class="central-meta bg-light">
            <div class="new-postbox">
                <form action="/posts" method="post" enctype="multipart/form-data">
                    @csrf
                    <textarea rows="2" placeholder="Type your post here..." name="story"></textarea>
                    <div class="attachments">
                        <ul>
                            <li>
                                <i class="fa fa-image"></i>
                                <label class="fileContainer">
                                    <input type="file" name="img">
                                </label>
                            </li>
                            <li>
                                <button class="btn btn-primary" type="submit">Share Post</button>
                            </li>
                        </ul>
                    </div>
                </form>
             </div>
        </div><!-- Buat textbox posting -->

        @foreach ($posts as $post)
            <div class="loadMore bg-light">
                <div class="central-meta item">
                    <div class="user-post">
                        <div class="friend-info">
                            <figure>
                                <img src="{{asset('images/'.$post->picture)}}" alt="photo_profile">
                            </figure>
                            <div class="friend-name">
                                <ins><a href="/users/{{$post->users_id}}">{{$post->fname}} {{$post->lname}}</a></ins>
                                <span>{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</span>
                            </div>
                            <div class="post-meta">
                                <img src="{{asset('images/'.$post->img)}}" alt="image_post">
                                <div class="we-video-info" style="display: flex;">
                                    <a href="/posts/{{$post->post_id}}" class="btn btn-success btn-lg mr-1">Comments</a>
                                    <a href="/likepost/{{$post->post_id}}" class="btn btn-dark btn-lg mr-1">Likes 
                                        @if ($post->countlike==null)
                                            (0)
                                        @else
                                        ({{$post->countlike}})
                                        @endif
                                    </a>
                                    @if (Auth::user()->id==$post->users_id)
                                    <a href="/posts/{{$post->post_id}}/edit" class="btn btn-primary btn-lg mr-1">Edit</a>
                                    <form method="post" action="/destroy_postuser/{{$post->id}}">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-lg">Delete</button>
                                    </form>
                                    @endif
                                </div>
                                <div class="description">
                                    <p style="font-size: 20px;">
                                    {!!$post->story!!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="row mt-3">
                <div class="col">
                    <div class="d-flex justify-content-center">
                        {!! $posts->links() !!}
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
