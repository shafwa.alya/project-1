@extends('layouts.app')

@section('content')
<h1>{{$user->username}}</h1>
<hr>
<div class="row">
    <div class="col-md-12">
        <form action="/users/{{$user->id}}" method="post" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="fname">Front Name</label>
                        <input type="text" name="fname" id="fname" class="form-control" value="{{$user->fname}}">
                        @error('fname')
                            <span class="text-danger" style="font-size: 12px;">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lname">Last Name</label>
                        <input type="text" name="lname" id="lname" class="form-control" value="{{$user->lname}}">
                        @error('lname')
                            <span class="text-danger" style="font-size: 12px;">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" id="username" class="form-control" value="{{$user->username}}">
                        @error('username')
                            <span class="text-danger" style="font-size: 12px;">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control" value="{{$user->email}}">
                        @error('email')
                            <span class="text-danger" style="font-size: 12px;">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="sex">Gender</label>
                        <select id="gender" name="sex" class="form-control">
                            <option value="male"
                                @if($user->sex=="male")
                                selected
                                @endif>Male</option>   
                            <option value="female"
                                @if($user->sex=="female")
                                selected
                                @endif>Female</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="birthday">Birthday</label>
                        <input type="date" name="birthday" id="birthday" class="form-control" value="{{$user->birthday}}">
                        @error('birthday')
                            <span class="text-danger" style="font-size: 12px;">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="picture">Photo Profile</label>
                        <input type="file" name="picture" id="picture" onchange="previewImg()" class="form-control">
                        @error('picture')
                            <span class="text-danger" style="font-size: 12px;">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <label>Preview Photo Profile</label>
                    <div class="card col-md-12 mb-4">
                        <div class="card-body">
                            <div style="text-align: center;">
                                @if ($user->picture!=NULL)
                                <img src="{{ asset('images/'.$user->picture) }}" class="img img-responsive img-preview" width="200px">
                                @else
                                <img src="{{ asset('images/defaultprofile.png') }}" class="img img-responsive img-preview" width="200px">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function previewImg() {
        const picture = document.querySelector('#picture');
        const imgPreview = document.querySelector('.img-preview');
        const filePicture = new FileReader();
        filePicture.readAsDataURL(picture.files[0]);

        filePicture.onload = function(e) {
            imgPreview.src = e.target.result;
        }
    }
</script>
@endsection
