<!-- Button trigger modal -->
<button type="button" class="btn btn-danger btn-block mt-2" data-toggle="modal" data-target="#delete_account">
    Delete Account
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="delete_account" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete Account</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/users/{{$user->id}}" method="post">
          @method('delete')
          @csrf
        <div class="modal-body">
         Are you sure want to delete your account? 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Delete</button>
        </div>
      </form>
      </div>
    </div>
  </div>