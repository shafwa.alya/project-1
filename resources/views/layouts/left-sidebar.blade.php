    <aside class="sidebar static">
        <div class="widget">
            <h4 class="widget-title">Shortcuts</h4>
            <ul class="naves">
                <li>
                    <i class="ti-clipboard"></i>
                    <a href="newsfeed.html" title="">Feeds</a>
                </li>
                <li>
                    <i class="ti-mouse-alt"></i>
                    <a href="inbox.html" title="">Pesan</a>
                </li>
                <li>
                    <i class="ti-user"></i>
                    <a href="timeline-friends.html" title="">Teman</a>
                </li>
                <li>
                    <i class="ti-share"></i>
                    <a href="people-nearby.html" title="">Sekitarku</a>
                </li>
                <li>
                    <i class="fa fa-bar-chart-o"></i>
                    <a href="insights.html" title="">Insights</a>
                </li>
                <li>
                    <i class="ti-power-off"></i>
                    <a href="landing.html" title="">Logout</a>
                </li>
            </ul>
        </div><!-- Shortcuts -->