<div class="topbar stick">
    <div class="logo">
        <a title="SharePost" style="font-size: auto;background: -webkit-linear-gradient(rgb(132, 107, 243), rgb(241, 245, 15));-webkit-background-clip: text;-webkit-text-fill-color: transparent;"><b>SharePost</b></a>
    </div>
    
    <div class="top-area">
        <ul class="main-menu">
            <li>
                <a href="#" title="">Personal</a>
                <ul>
                    <li><a href="#" title="">Beranda</a></li>
                    <li><a href="#" title="">Profil</a></li>
                </ul>
            </li>
            <li>
                <a href="#" title="">Komunitas</a>
                <ul>
                    <li><a href="#" title="">Orang Terdekat</a></li>
                    <li><a href="#" title="">Trending</a></li>
                </ul>
            </li>
            <li>
                <a href="#" title="">Dunia</a>
                <ul>
                    <li><a href="#" title="">Berita</a></li>
                    <li><a href="#" title="">Lifehack</a></li>
                    <li><a href="#" title="">Kreatif</a></li>
                </ul>
            </li>
            <li>
                <a href="#" title="">Pengaturan</a>
                <ul>
                    <li><a href="#" title="">Pengaturan Akun</a></li>
                    <li><a href="#" title="">FAQ</a></li>
                </ul>
            </li>
        </ul>
        <ul class="setting-area">
            <li>
                <a href="#" title="Home" data-ripple=""><i class="ti-search"></i></a>
                <div class="searched">
                    <form method="post" class="form-search">
                        <input type="text" placeholder="Search Friend">
                        <button data-ripple><i class="ti-search"></i></button>
                    </form>
                </div>
            </li>
            <li><a href="newsfeed.html" title="Home" data-ripple=""><i class="ti-home"></i></a></li>
            <li>
                <a href="#" title="Notification" data-ripple="">
                    <i class="ti-bell"></i><span>20</span>
                </a>
                <div class="dropdowns">
                    <span>4 New Notifications</span>
                    <ul class="drops-menu">
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-1.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>sarah Loren</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag green">New</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-2.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>Jhon doe</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag red">Reply</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-3.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>Andrew</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag blue">Unseen</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-4.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>Tom cruse</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag">New</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-5.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>Amy</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag">New</span>
                        </li>
                    </ul>
                    <a href="notifications.html" title="" class="more-mesg">view more</a>
                </div>
            </li>
            <li>
                <a href="#" title="Messages" data-ripple=""><i class="ti-comment"></i><span>12</span></a>
                <div class="dropdowns">
                    <span>5 New Messages</span>
                    <ul class="drops-menu">
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-1.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>sarah Loren</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag green">New</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-2.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>Jhon doe</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag red">Reply</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-3.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>Andrew</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag blue">Unseen</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-4.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>Tom cruse</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag">New</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <img src="images/resources/thumb-5.jpg" alt="">
                                <div class="mesg-meta">
                                    <h6>Amy</h6>
                                    <span>Hi, how r u dear ...?</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag">New</span>
                        </li>
                    </ul>
                    <a href="messages.html" title="" class="more-mesg">view more</a>
                </div>
            </li>
        </ul>
        <div class="user-img">
            <img src="images/resources/admin.jpg" alt="">
            <span class="status f-online"></span>
            <div class="user-setting">
                <a href="#" title=""><span class="status f-online"></span>online</a>
                <a href="#" title=""><span class="status f-away"></span>away</a>
                <a href="#" title=""><span class="status f-off"></span>offline</a>
                <a href="#" title=""><i class="ti-user"></i> view profile</a>
                <a href="#" title=""><i class="ti-pencil-alt"></i>edit profile</a>
                <a href="#" title=""><i class="ti-target"></i>activity log</a>
                <a href="#" title=""><i class="ti-settings"></i>account setting</a>
                <a href="#" title=""><i class="ti-power-off"></i>log out</a>
            </div>
        </div>
    </div>
</div><!-- topbar -->