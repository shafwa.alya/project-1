<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title>SharePost Share Your Story</title>
    <link rel="icon" href="{{asset('images/fav.png')}}" type="image/png" sizes="16x16"> 
    
    <link rel="stylesheet" href="{{asset('css/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/color.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

</head>
<body>
<!--<div class="se-pre-con"></div>-->
<div class="theme-layout">
	<div class="container-fluid pdng0">
		<div class="row merged">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="land-featurearea">
					<div class="land-meta">
						<h1>SharePost</h1>
						<p>
							"SharePost" Berbagi ceritamu hari ini bersama komunitas
						</p>
						<div class="friend-logo">
							<span><img src="images/wink.png" alt=""></span>
						</div>
						<a href="#" title="" class="folow-me">Trending di Komunitas</a>
					</div>	
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="login-reg-bg">
					<div class="log-reg-area sign">
						<h2 class="log-title">Login</h2>
						<form method="post" action="{{ route('login') }}">
							@csrf
							<div class="form-group">	
							  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
							  <label class="control-label" for="email">Email</label><i class="mtrl-select"></i>
							</div>
							@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
							<div class="form-group">	
							  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"/>
							  <label class="control-label" for="email">Password</label><i class="mtrl-select"></i>
							</div>
							@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
							
							
							<div class="submit-btns">
								<button class="mtr-btn signin" type="submit"><span>Masuk</span></button>
								<button class="mtr-btn signup" type="button"><span>Daftar</span></button>
							</div>
						</form>
					</div>
					<div class="log-reg-area reg">
						<h2 class="log-title">Register</h2>
						<form method="post" action="{{ route('register') }}">
							@csrf
							<div class="form-group">	
							  <input id="fname" type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" required autocomplete="fname" autofocus/>
							  <label class="control-label" for="input">Nama Depan</label><i class="mtrl-select"></i>
							</div>
							@error('fname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
							<div class="form-group">	
							  <input id="lname" type="text" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" required autocomplete="lname" autofocus/>
							  <label class="control-label" for="input">Nama Belakang</label><i class="mtrl-select"></i>
							</div>
							@error('lname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
							<div class="form-group">	
								<input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus/>
								<label class="control-label" for="input">Username</label><i class="mtrl-select"></i>
							</div>
							@error('username')
									<span class="invalid-feedback" role="alert">
									  <strong>{{ $message }}</strong>
									</span>
							@enderror
							<div class="form-group">	
								<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"/>
								<label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
							</div>
							@error('email')
									<span class="invalid-feedback" role="alert">
									  <strong>{{ $message }}</strong>
									</span>
							@enderror
							<div class="form-group">
							  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"/>
							  <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
							</div>
							@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
							<div class="form-group">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"/>
								<label class="control-label" for="input">Ketik Ulang Password</label><i class="mtrl-select"></i>
							</div>
							<div class="form-radio">
							  <div class="radio">
								<label>
								  <input type="radio" name="sex" value="male"/><i class="check-box"></i>Laki-laki
								</label>
							  </div>
							  <div class="radio">
								<label>
								  <input type="radio" name="sex" value="female"/><i class="check-box"></i>Perempuan
								</label>
							  </div>
							</div>
							@error('sex')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
							<div class="form-group">
							  <input id="birthday" type="date" class="form-control @error('birthday') is-invalid @enderror" name="birthday" value="{{ old('birthday') }}" required autocomplete="birthday" autofocus/>
							  <label class="control-label" for="input">Tanggal Lahir</label><i class="mtrl-select"></i>
							</div>
							@error('birthday')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
							<div class="submit-btns">
								<button class="mtr-btn" type="submit"><span>Daftar</span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
	<script data-cfasync="false" src="{{asset('../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script><script src="js/main.min.js"></script>
	<script src="{{asset('js/script.js')}}"></script>

</body>	

</html>
