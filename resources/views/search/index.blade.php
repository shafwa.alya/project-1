@extends('layouts.app')

@section('content')
<h1>Search</h1>
<hr>
<div class="row justify-content-center">
    <div class="col-md-12">
        <form action="/search">
            <div class="input-group mb-3">
                <input type="text" class="form-control form-control-lg" placeholder="Search Username..." aria-label="Search Username..." aria-describedby="basic-addon2" name="searchuser" value="{{request('searchuser')}}">
                <div class="input-group-append">
                  <button class="btn btn-outline-secondary btn-lg" type="submit"><i class="fa fa-search"></i></button>
                </div>
              </div>
        </form>
    </div>
</div>

<div class="row">
    @foreach ($users as $user)
    <div class="col-md-3 mb-3">
        <div class="card">
            <a href="/users/{{$user->id}}" class="card_profile">
            <img class="card-img-top" src="{{asset('images/'.$user->picture)}}" alt="Card image cap" style="height: 180px;">
            <div class="card-body">
              <h5 class="card-title text-center">{{$user->fname}} {{$user->lname}}</h5>
            </div>
            </a>
            @if ($user->my_id==Auth::user()->id && $user->followed_id==$user->users_id)
                <a href="/follow/unfollow/{{$user->users_id}}" class="btn btn-warning btn-block">Unfolow</a>
            @else
                @if ($user->id==Auth::user()->id)
                <button type="button" class="btn btn-secondary btn-block" disabled>Follow</button>
                @else
                <a href="/follow/following/{{$user->users_id}}" class="btn btn-success btn-block">Follow</a>
                @endif
            @endif
        </div>
    </div>
    @endforeach
</div>

<div class="row mt-3">
    <div class="col">
        <div class="d-flex justify-content-center">
            {!! $users->links() !!}
        </div>
    </div>
</div>
@endsection
