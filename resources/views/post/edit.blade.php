@extends('layouts.app')

@section('content')
<h1>Edit Post</h1>
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="central-meta bg-light">
            <div class="new-postbox">
                <figure>
                    <img src="{{asset('images/'.$user->picture)}}" alt="photo_profile">
                </figure>
                <div class="newpst-input">
                    <form action="/posts/{{$post->id}}" method="post" enctype="multipart/form-data">
                        @method('put')
                        @csrf
                        <textarea rows="2" placeholder="Type your post here..." name="story">{{$post->story}}</textarea>
                        <div class="attachments">
                            <ul>
                                <li>
                                    <i class="fa fa-image"></i>
                                    <label class="fileContainer">
                                        <input type="file" name="img" id="img" onchange="previewImg()">
                                    </label>
                                </li>
                                <li>
                                    <button class="btn btn-primary" type="submit">Edit Post</button>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
             </div>
        </div><!-- Buat textbox posting -->

        <div class="loadMore bg-light">
            <div class="central-meta item">
                <div class="user-post">
                    <div class="friend-info">
                        <div class="post-meta">
                            <img src="{{asset('images/'.$post->img)}}" alt="image_post" class="img-preview">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function previewImg() {
        const img = document.querySelector('#img');
        const imgPreview = document.querySelector('.img-preview');
        const filePicture = new FileReader();
        filePicture.readAsDataURL(img.files[0]);

        filePicture.onload = function(e) {
            imgPreview.src = e.target.result;
        }
    }
</script>
@endsection
