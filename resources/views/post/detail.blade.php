@extends('layouts.app')

@section('content')
<h1>Detail Post</h1>
<hr>
<div class="loadMore bg-light">
    <div class="central-meta item">
        <div class="user-post">
            <div class="friend-info">
                <figure>
                    <img src="{{asset('images/'.$post->picture)}}" alt="photo_profile">
                </figure>
                <div class="friend-name">
                    <ins><a href="/users/{{$post->users_id}}">{{$post->fname}} {{$post->lname}}</a></ins>
                    <span>{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</span>
                </div>
                <div class="post-meta">
                    <img src="{{asset('images/'.$post->img)}}" alt="image_post">
                    <div class="we-video-info" style="display: flex;">
                        <a href="/posts/{{$post->id}}" class="btn btn-success btn-lg mr-1">Comments ({{$comments_count}})</a>
                        <a href="/likepost/{{$post->id}}" class="btn btn-dark btn-lg mr-1">Likes @if ($likes->count_likes==null)
                            (0)
                        @else
                        ({{$likes->count_likes}})
                        @endif</a>
                        @if (Auth::user()->id==$post->users_id)
                        <a href="/posts/{{$post->id}}/edit" class="btn btn-primary btn-lg mr-1">Edit</a>
                        <form method="post" action="/posts/{{$post->id}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-lg">Delete</button>
                        </form>
                        @endif
                    </div>
                    <div class="description">
                        <p style="font-size: 20px;">
                            {!!$post->story!!}
                         </p>
                    </div>
                </div>
            </div>
            <div class="coment-area">
                <ul class="we-comet">
                    <li class="post-comment">
                        <form method="post" action="/comments">
                            @csrf
                            <textarea placeholder="Post your comment" name="comment_field" required></textarea>
                            <input type="hidden" name="post_id" value="{{$post->id}}">
                            <button type="submit" class="btn btn-success">Comment</button>
                        </form>
                    </li>
                    @foreach ($comments as $comment)
                    <li class="post-comment">
                        <div class="we-comment" style="width: 1200px;">
                            <div class="coment-head">
                                <h5><a href="time-line.html" title="">{{$comment->fname}} {{$comment->lname}}</a></h5>
                                <span>{{ Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</span>
                            </div>
                            <p>{{$comment->comment_field}}</p>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div><!-- centerl meta -->
@endsection
