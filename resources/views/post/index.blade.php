@extends('layouts.app')
@section('content')
<h1>All Posts</h1>
<hr>
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif
@foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $error }}</strong>
    </div>
@endforeach
<div class="central-meta bg-light">
    <div class="new-postbox">
        <figure>
            <img src="{{asset('images/'.$user->picture)}}" alt="photo_profile">
        </figure>
        <div class="newpst-input">
            <form action="/posts" method="post" enctype="multipart/form-data">
                @csrf
                <textarea rows="2" placeholder="Type your post here..." name="story"></textarea>
                <div class="attachments">
                    <ul>
                        <li>
                            <i class="fa fa-image"></i>
                            <label class="fileContainer">
                                <input type="file" name="img">
                            </label>
                        </li>
                        <li>
                            <button class="btn btn-primary" type="submit">Share Post</button>
                        </li>
                    </ul>
                </div>
            </form>
        </div>
     </div>
</div><!-- Buat textbox posting -->

@foreach ($posts as $post)
<div class="loadMore bg-light">
    <div class="central-meta item">
        <div class="user-post">
            <div class="friend-info">
                <figure>
                    <img src="{{asset('images/'.$post->picture)}}" alt="photo_profile">
                </figure>
                <div class="friend-name">
                    <ins><a href="/users/{{$post->users_id}}">{{$post->fname}} {{$post->lname}}</a></ins>
                    <span>{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</span>
                </div>
                <div class="post-meta">
                    <img src="{{asset('images/'.$post->img)}}" alt="image_post">
                    <div class="we-video-info" style="display: flex;">
                        <a href="/posts/{{$post->post_id}}" class="btn btn-success btn-lg mr-1">Comments</a>
                        <a href="/likepost/{{$post->post_id}}" class="btn btn-dark btn-lg mr-1">Likes 
                            @if ($post->countlike==null)
                                (0)
                            @else
                            ({{$post->countlike}})
                            @endif
                        </a>
                        @if (Auth::user()->id==$post->users_id)
                        <a href="/posts/{{$post->post_id}}/edit" class="btn btn-primary btn-lg mr-1">Edit</a>
                        <form method="post" action="/posts/{{$post->id}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-lg">Delete</button>
                        </form>
                        @endif
                    </div>
                    <div class="description">
                        <p style="font-size: 20px;">
                           {!!$post->story!!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

<div class="row mt-3">
    <div class="col">
        <div class="d-flex justify-content-center">
            {!! $posts->links() !!}
        </div>
    </div>
</div>
<!-- centerl meta -->
@endsection
