<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::resource('posts', 'PostController');
Route::get('/search', 'SearchController@index');
Route::get('/follow/following/{id}', 'FollowController@following');
Route::get('/follow/unfollow/{id}', 'FollowController@unfollow');
// Route::get('/follow/userfollowing/{id}', 'FollowController@userfollowing');
// Route::get('/follow/userfollowers/{id}', 'FollowController@userfollowers');
Route::resource('users', 'UserController');
Route::put('/update_password/{id}', 'UserController@update_password')->name('update_password');
Route::resource('friends', 'FriendController');
Route::get('/likepost/{id}', 'PostController@likepost')->name('likepost');
Route::resource('comments', 'CommentController');
Route::delete('/destroy_postuser/{id}', 'PostController@destroy_postuser');
