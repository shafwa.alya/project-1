<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $auth_id = Auth::user()->id;

        $users = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.users_id')
            ->joinSub("select * from follows where users_id=$auth_id", 'follows', 'users.id', '=', 'follows.profile_id', 'left')
            ->select('users.*', 'profiles.*', 'follows.users_id as my_id', 'follows.profile_id as followed_id')
            ->paginate(8);

        if (request('searchuser')) {
            $users = DB::table('users')
                ->join('profiles', 'users.id', '=', 'profiles.users_id')
                ->joinSub("select * from follows where users_id=$auth_id", 'follows', 'users.id', '=', 'follows.profile_id', 'left')
                ->select('users.*', 'profiles.*', 'follows.users_id as my_id', 'follows.profile_id as followed_id')
                ->where('username', 'like', '%' . request('searchuser') . '%')
                ->paginate(8);
        }

        return view('search.index', compact('users'));
    }
}
