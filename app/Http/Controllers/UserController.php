<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.users_id')
            ->select('users.*', 'profiles.*')
            ->where('users.id', $id)
            ->first();

        $checkfollowing = DB::table('follows')->where('users_id', $id)->count();
        if ($checkfollowing > 0) {
            $following = DB::table('follows')
                ->select(DB::raw('count(profile_id) as count_following'))
                ->where('users_id', $id)
                ->groupBy('users_id')
                ->first();
            $count_following = $following->count_following;
        } else {
            $count_following = 0;
        }

        $checkfollowers = DB::table('follows')->where('profile_id', $id)->count();
        if ($checkfollowers > 0) {
            $followers = DB::table('follows')
                ->select(DB::raw('count(users_id) as count_followers'))
                ->where('profile_id', $id)
                ->first();
            $count_followers = $followers->count_followers;
        } else {
            $count_followers = 0;
        }

        $checkfollow = DB::table('follows')
            ->where('users_id', Auth::user()->id)
            ->where('profile_id', $id)
            ->count();

        $posts = DB::table('tablelike')
            ->join('posts', 'tablelike.post_id', '=', 'posts.id')
            ->join('profiles', 'posts.users_id', '=', 'profiles.users_id')
            ->select('posts.*', 'profiles.fname', 'profiles.lname', 'profiles.picture', DB::raw('sum(count_like) as countlike'), 'tablelike.post_id', 'tablelike.profile_id')
            ->where('posts.users_id', $id)
            ->groupBy('post_id')
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        return view('user.detail', ['user' => $user, 'count_following' => $count_following, 'count_followers' => $count_followers, 'checkfollow' => $checkfollow, 'posts' => $posts]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.users_id')
            ->select('users.*', 'profiles.*', 'profiles.id as idprofile')
            ->where('users.id', $id)
            ->first();
        return view('user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email,' . $id,
            'username' => 'required|unique:users,username,' . $id,
            'fname' => 'required',
            'lname' => 'required',
            'birthday' => 'required',
            'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('picture')) {

            $user = User::find($id);
            $profile = DB::table('profiles')->where('users_id', $user->id)->first();
            if ($profile->picture != "defaultprofile.png") {
                $old_image = public_path() . "/images/" . $profile->picture;
                @unlink($old_image);
            }

            $imageName = time() . '.' . $request->picture->extension();
            $request->picture->move(public_path('images'), $imageName);

            $user->update([
                'username' => $request->username,
                'email' => $request->email
            ]);

            $profile = Profile::find($id);
            $profile->update([
                'fname' => $request->fname,
                'lname' => $request->lname,
                'sex' => $request->sex,
                'birthday' => $request->birthday,
                'picture' => $imageName
            ]);

            return redirect("/users/$id");
        } else {
            $user = User::find($id);
            $user->update([
                'username' => $request->username,
                'email' => $request->email
            ]);

            $profile = Profile::find($id);
            $profile->update([
                'fname' => $request->fname,
                'lname' => $request->lname,
                'sex' => $request->sex,
                'birthday' => $request->birthday
            ]);

            return redirect("/users/$id");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = DB::table('profiles')->where('users_id', $id)->first();
        if ($profile->picture != "defaultprofile.png") {
            $old_image = public_path() . "/images/" . $profile->picture;
            @unlink($old_image);
        }

        DB::table('profiles')->where('users_id', $id)->delete();
        DB::table('follows')->where('users_id', $id)->delete();
        DB::table('follows')->where('profile_id', $id)->delete();
        DB::table('posts')->where('users_id', $id)->delete();
        DB::table('tablelike')->where('profile_id', $id)->delete();
        DB::table('comments')->where('profile_id', $id)->delete();

        $user = User::find($id);
        $user->delete();

        Auth::logout();
        return redirect("/");
    }

    public function update_password(Request $request, $id)
    {
        $request->validate([
            'password' =>  'required|min:8|confirmed'
        ]);

        $user = User::find($id);
        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return redirect("/users/$id")->with('success', 'Success updated password!');
    }
}
