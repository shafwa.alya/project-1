<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    public function index()
    {
        $user_count = User::count();
        return view('home', ['user_count' => $user_count]);
    }
}
