<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Follow;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function following($profile_id)
    {
        Follow::create([
            'users_id' => Auth::user()->id,
            'profile_id' => $profile_id
        ]);
        return back();
    }

    public function unfollow($profile_id)
    {
        DB::table('follows')->where('users_id', Auth::user()->id)->where('profile_id', $profile_id)->delete();
        return back();
    }

    // public function userfollowing($auth_id)
    // {
    //     $users = DB::table('users')
    //         ->join('profiles', 'users.id', '=', 'profiles.users_id')
    //         ->joinSub("select * from follows where users_id=$auth_id", 'follows', 'users.id', '=', 'follows.profile_id', 'left')
    //         ->select('users.*', 'profiles.*', 'follows.users_id as my_id', 'follows.profile_id as followed_id')
    //         ->where('follows.users_id', $auth_id)
    //         ->paginate(8);

    //     if (request('searchuser')) {

    //         $users = DB::table('users')
    //             ->join('profiles', 'users.id', '=', 'profiles.users_id')
    //             ->joinSub("select * from follows where users_id=$auth_id", 'follows', 'users.id', '=', 'follows.profile_id', 'left')
    //             ->select('users.*', 'profiles.*', 'follows.users_id as my_id', 'follows.profile_id as followed_id')
    //             ->where('follows.users_id', $auth_id)
    //             ->where('username', 'like', '%' . request('searchuser') . '%')
    //             ->paginate(8);
    //     }

    //     $checkfollowing = DB::table('follows')->where('users_id', $auth_id)->count();
    //     if ($checkfollowing > 0) {
    //         $following = DB::table('follows')
    //             ->select(DB::raw('count(profile_id) as count_following'))
    //             ->where('users_id', $auth_id)
    //             ->groupBy('users_id')
    //             ->first();
    //         $count_following = $following->count_following;
    //     } else {
    //         $count_following = 0;
    //     }

    //     return view('follow.userfollowing', ['users'=>$users, 'auth_id'=>$auth_id, 'count_following'=>$count_following]);
    // }

    // public function userfollowers($auth_id)
    // {
    //     $users = DB::table('users')
    //         ->join('profiles', 'users.id', '=', 'profiles.users_id')
    //         ->joinSub("select * from follows where users_id=$auth_id", 'follows', 'users.id', '=', 'follows.profile_id', 'left')
    //         ->select('users.*', 'profiles.*', 'follows.users_id as my_id', 'follows.profile_id as followed_id')
    //         ->where('follows.profile_id', '=', $auth_id)
    //         ->paginate(8);

    //     if (request('searchuser')) {

    //         $users = DB::table('users')
    //             ->join('profiles', 'users.id', '=', 'profiles.users_id')
    //             ->joinSub("select * from follows where users_id=$auth_id", 'follows', 'users.id', '=', 'follows.profile_id', 'left')
    //             ->select('users.*', 'profiles.*', 'follows.users_id as my_id', 'follows.profile_id as followed_id')
    //             ->where('follows.profile_id', $auth_id)
    //             ->where('username', 'like', '%' . request('searchuser') . '%')
    //             ->paginate(8);
    //     }

    //     $checkfollowers = DB::table('follows')->where('profile_id', $auth_id)->count();
    //     if ($checkfollowers > 0) {
    //         $followers = DB::table('follows')
    //             ->select(DB::raw('count(users_id) as count_followers'))
    //             ->where('profile_id', $auth_id)
    //             ->first();
    //         $count_followers = $followers->count_followers;
    //     } else {
    //         $count_followers = 0;
    //     }

    //     return view('follow.userfollowers', ['users'=>$users, 'auth_id'=>$auth_id, 'count_followers'=>$count_followers]);
    // }
}
