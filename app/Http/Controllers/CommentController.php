<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Comment;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        // ['profile_id', 'post_id', 'comment_field'];

        Comment::create([
            'profile_id' => Auth::user()->id,
            'post_id' => $request->post_id,
            'comment_field' => $request->comment_field
        ]);
        return back();
    }
}
