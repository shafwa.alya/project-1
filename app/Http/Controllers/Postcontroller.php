<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\Tablelike;

class Postcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = DB::table('profiles')->where('users_id', Auth::user()->id)->first();
        $posts = DB::table('tablelike')
            ->join('posts', 'tablelike.post_id', '=', 'posts.id')
            ->join('profiles', 'posts.users_id', '=', 'profiles.users_id')
            ->select('posts.*', 'profiles.fname', 'profiles.lname', 'profiles.picture', DB::raw('sum(count_like) as countlike'), 'tablelike.post_id', 'tablelike.profile_id')
            ->groupBy('post_id')
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        return view('post.index', ['user' => $user, 'posts' => $posts]);
    }

    public function likepost($post_id)
    {
        $likecheck = DB::table('tablelike')->where('post_id', $post_id)->where('profile_id', Auth::user()->id)->count();
        if ($likecheck < 1) {
            Tablelike::create([
                'profile_id' => Auth::user()->id,
                'post_id' => $post_id,
                'count_like' => 1
            ]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'story' => 'required',
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|required',
        ]);

        $imageName = time() . '.' . $request->img->extension();
        $request->img->move(public_path('images'), $imageName);

        $post = Post::create([
            'img' => $imageName,
            'story' => $request->story,
            'users_id' => Auth::user()->id
        ]);

        Tablelike::create([
            'post_id' => $post->id
        ]);

        return back()->with('success', 'Success share yuor story!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = DB::table('posts')
            ->join('profiles', 'posts.users_id', '=', 'profiles.users_id')
            ->select('posts.*', 'profiles.fname', 'profiles.lname', 'profiles.picture')
            ->where('posts.id', $id)
            ->first();
        $likes  = DB::table('tablelike')
            ->select('*', DB::raw('sum(count_like) as count_likes'))
            ->where('post_id', $id)
            ->groupBy('post_id')
            ->first();
        $comments_count  = DB::table('comments')
            ->where('post_id', $id)
            ->groupBy('post_id')
            ->count();

        $comments = DB::table('comments')
            ->join('profiles', 'comments.profile_id', '=', 'profiles.users_id')
            ->select('comments.*', 'profiles.fname', 'profiles.lname')
            ->where('post_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('post.detail', ['post' => $post, 'likes' => $likes, 'comments_count' => $comments_count, 'comments' => $comments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = DB::table('profiles')->where('users_id', Auth::user()->id)->first();
        $post = Post::find($id);
        return view('post.edit', ['post' => $post, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'story' => 'required',
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('img')) {

            $post = Post::find($id);
            $old_image = public_path() . "/images/" . $post->img;
            @unlink($old_image);

            $imageName = time() . '.' . $request->img->extension();
            $request->img->move(public_path('images'), $imageName);

            $post->update([
                'story' => $request->story,
                'img' => $imageName
            ]);

            return redirect("/posts/" . $id)->with('success', 'Success edit your post!');
        } else {
            $post = Post::find($id);
            $post->update([
                'story' => $request->story
            ]);

            return redirect("/posts/" . $id)->with('success', 'Success edit your post!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $old_image = public_path() . "/images/" . $post->img;
        @unlink($old_image);
        DB::table('tablelike')->where('post_id', $post->id)->delete();
        DB::table('comments')->where('post_id', $post->id)->delete();

        $post = Post::find($id);
        $post->delete();

        return redirect("/posts")->with('success', 'Success delete your post!');
    }

    public function destroy_postuser($id)
    {
        $post = Post::find($id);
        $old_image = public_path() . "/images/" . $post->img;
        @unlink($old_image);
        DB::table('tablelike')->where('post_id', $post->id)->delete();
        DB::table('comments')->where('post_id', $post->id)->delete();

        $post = Post::find($id);
        $post->delete();

        return back();
    }
}
