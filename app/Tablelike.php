<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tablelike extends Model
{
    protected $table = 'tablelike';
    protected $fillable = ['post_id', 'profile_id', 'count_like', 'count_dislike'];
}
